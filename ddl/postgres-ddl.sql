create schema model1;

create table model1.department (
	dname		varchar		primary key
,	floor		integer
,	budget		integer
);

create table model1.employee (
	ename		varchar
,	age		integer
,	salary		integer
,	department	varchar		references model1.department(dname)
);


create schema model2;

create table model2.department (
        dname           varchar         primary key
,       floor           integer
,       budget          integer
);

create table model2.employee (
        ename           varchar         primary key
,       age             integer
,       salary          integer
);

create table model2.works_in (
        ename           varchar         references model2.employee (ename)
,       dname           varchar         references model2.department (dname)
,       dedication_pct  integer
);
