db.employee.insert([
  {
    "_id": "1",
    "ename": "Bill",
    "age": 36,
    "salary": 10000,
    "department": "1001"
  },
  {
    "_id": "2",
    "ename": "Sam",
    "age": 27,
    "salary": 15000,
    "department": "1002"
  },
  {
    "_id": "3",
    "ename": "Fred",
    "age": 29,
    "salary": 12000,
    "department": "1001"
  }
]);


db.department.insert([
  {
    "_id": "1001",
    "dname": "Shoe",
    "floor": 1,
    "budget": 1200
  },
  {
    "_id": "1002",
    "dname": "Toy",
    "floor": 2,
    "budget": 1400
  },
  {
    "_id": "1003",
    "dname": "Candy",
    "floor": 1,
    "budget": 900
  }
]);


db.employee.aggregate([
  {
    $lookup: {
      from: "department",
      localField: "department",
      foreignField: "_id",
      as: "dept"
    }
  },
  {
    $unwind: "$dept"
  },
  {
    $group: {
      "_id": "$dept.dname",
      "salary": { "$sum": "$salary" },
    }
  }
]);

{ "_id" : "Shoe", "totalsalary" : 22000 }
{ "_id" : "Toy", "totalsalary" : 15000 }
