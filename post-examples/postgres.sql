-- required DDL is in ddl/ folder

set search_path = model1;

insert into department values
	('Shoe', 1, 1200)
,	('Toy', 2, 1400)
,	('Candy', 1, 900)
;

insert into employee values
	('Bill', 36, 10000, 'Shoe')
,	('Sam', 27, 15000, 'Toy')
,	('Fred', 29, 12000, 'Shoe')
;

select		dname, sum(salary)
from		employee as e
			inner join
			department as d
			on e.department = d.dname
group by	dname
;

---┌───────┬───────┐
---│ dname │  sum  │
---├───────┼───────┤
---│ Shoe  │ 22000 │
---│ Toy   │ 15000 │
---└───────┴───────┘


select		dname, sum(coalesce(salary, 0))
from		employee as e
			right outer join
			department as d
			on e.department = d.dname
group by	dname
;

---┌───────┬───────┐
---│ dname │  sum  │
---├───────┼───────┤
---│ Toy   │ 15000 │
---│ Candy │     0 │
---│ Shoe  │ 22000 │
---└───────┴───────┘


----------

set search_path = model2;

insert into department values
        ('Shoe', 1, 1200)
,       ('Toy', 2, 1400)
,       ('Candy', 1, 900)
;

insert into employee values
        ('Bill', 36, 10000)
,       ('Sam', 27, 15000)
,       ('Fred', 29, 12000)
;

insert into Works_in values
        ('Bill', 'Toy', 60)
,       ('Bill', 'Shoe', 40)
,       ('Sam', 'Toy', 100)
,       ('Fred', 'Shoe', 100)
;

select		dname, sum(salary * dedication_pct * 0.01)
from		employee
			inner join
			works_in
			using (ename)
group by	dname
;

---┌───────┬──────────┐
---│ dname │   sum    │
---├───────┼──────────┤
---│ Shoe  │ 16000.00 │
---│ Toy   │ 21000.00 │
---└───────┴──────────┘


select		dname, sum(coalesce(salary * dedication_pct * 0.01, 0))
from		employee
			inner join
			works_in
			using (ename)
				right outer join
				department
				using (dname)
group by	dname
;

---┌───────┬──────────┐
---│ dname │   sum    │
---├───────┼──────────┤
---│ Shoe  │ 16000.00 │
---│ Toy   │ 21000.00 │
---│ Candy │        0 │
---└───────┴──────────┘
