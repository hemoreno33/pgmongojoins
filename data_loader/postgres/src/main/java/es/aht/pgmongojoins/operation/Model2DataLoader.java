package es.aht.pgmongojoins.operation;


import es.aht.pgmongojoins.util.BatchManager;
import es.aht.pgmongojoins.util.SQLExceptionSupplier;
import es.aht.pgmongojoins.util.ThreadBatchExecutor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;


public class Model2DataLoader extends AbstractDataLoader {
    private final int ONE_EVERY_X_EMPLOYEES_NOT_ASSIGNED_DEPARTMENT = 40;

    private final String INSERT_DEPARTMENT_SQL = "insert into model2.department (dname, floor, budget) values(?, ?, ?)";
    private final String INSERT_EMPLOYEE_SQL = "insert into model2.employee (ename, age, salary) values(?, ?, ?)";
    private final String INSERT_WORKSIN_SQL = "insert into model2.works_in (ename, dname, dedication_pct) values(?, ?, ?)";

    public Model2DataLoader(Properties properties, SQLExceptionSupplier<Connection> connectionSupplier) {
        super(properties, connectionSupplier);
    }

    @Override
    public void generateData() throws SQLException {
        generateDepartments();
        generateEmployees();
        generateWorksIn();
    }

    private void generateDepartments() throws SQLException {
        ThreadBatchExecutor.ThreadBatchConsumer<PreparedStatement> departmentThreadGenerator =
                (prepared, i, batchManager) -> {
                    int pos = 0;
                    prepared.setString(++pos, departmentName(i));
                    prepared.setInt(++pos, randomPositiveInt(MAX_FLOORS));
                    prepared.setInt(++pos, randomPositiveInt(MAX_BUDGET));
                    batchManager.submit();
                };

        generateInThreadBatch(
                connectionSupplier, propertiesConfiguration.departments(), INSERT_DEPARTMENT_SQL,
                departmentThreadGenerator
        );
    }

    private void generateEmployees() throws SQLException {
        ThreadBatchExecutor.ThreadBatchConsumer<PreparedStatement> employeesThreadGenerator =
                (prepared, i, batchManager) -> {
                        int pos = 0;
                        prepared.setString(++pos, employeeName(i));
                        prepared.setInt(++pos, randomPositiveInt(MAX_AGE));
                        prepared.setInt(++pos, randomPositiveInt(MAX_SALARY));
                        batchManager.submit();
                };

        generateInThreadBatch(
                connectionSupplier, propertiesConfiguration.employees(), INSERT_EMPLOYEE_SQL,
                employeesThreadGenerator
        );
    }

    private void generateWorksIn() throws SQLException {
        ThreadBatchExecutor.ThreadBatchConsumer<PreparedStatement> worksInThreadGenerator =
                (prepared, i, batchManager) -> generateWorksInEntry(prepared, employeeName(i), batchManager);

        generateInThreadBatch(
                connectionSupplier, propertiesConfiguration.employees(), INSERT_WORKSIN_SQL,
                worksInThreadGenerator
        );
    }

    private void generateWorksInEntry(PreparedStatement prepared, String ename, BatchManager batchManager)
            throws SQLException {
        // one in every X employees will not be assigned to any department
        if (randomPositiveInt(ONE_EVERY_X_EMPLOYEES_NOT_ASSIGNED_DEPARTMENT) == 1) {
            return;
        }

        int numberDepartments = randomPositiveInt(propertiesConfiguration.maxWorkInDepartments());

        int[] percentages = new int[numberDepartments];
        int accumulatedSum = 0;
        for (int i = 0; i < numberDepartments - 1; i++) {
            percentages[i] = randomPositiveInt(100 / numberDepartments);
            accumulatedSum += percentages[i];
        }
        percentages[percentages.length - 1] = 100 - accumulatedSum;

        for (int i = 0; i < numberDepartments; i++) {
            int pos = 0;
            prepared.setString(++pos, ename);
            prepared.setString(++pos, this.departmentName(randomPositiveInt(propertiesConfiguration.departments())));
            prepared.setInt(++pos, percentages[i]);
            batchManager.submit();
        }
    }
}
