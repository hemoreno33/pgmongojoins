package es.aht.pgmongojoins.operation;


import es.aht.pgmongojoins.util.BatchManager;
import es.aht.pgmongojoins.util.SQLExceptionSupplier;
import es.aht.pgmongojoins.util.ThreadBatchExecutor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;


public class Model1DataLoader extends AbstractDataLoader {
    private final String INSERT_DEPARTMENT_SQL = "insert into model1.department (dname, floor, budget) values(?, ?, ?)";
    private final String INSERT_EMPLOYEE_SQL = "insert into model1.employee (ename, age, salary, department) values(?, ?, ?, ?)";

    public Model1DataLoader(Properties properties, SQLExceptionSupplier<Connection> connectionSupplier) {
        super(properties, connectionSupplier);
    }

    @Override
    public void generateData() throws SQLException {
        generateDepartments();
        generateEmployees();
    }

    private void generateDepartments() throws SQLException {
        ThreadBatchExecutor.ThreadBatchConsumer<PreparedStatement> departmentThreadGenerator =
                (prepared, i, batchManager) -> {
                        int pos = 0;
                        prepared.setString(++pos, departmentName(i));
                        prepared.setInt(++pos, randomPositiveInt(MAX_FLOORS));
                        prepared.setInt(++pos, randomPositiveInt(MAX_BUDGET));
                        batchManager.submit();
                };

        generateInThreadBatch(
                connectionSupplier, propertiesConfiguration.departments(), INSERT_DEPARTMENT_SQL,
                departmentThreadGenerator
        );
    }

    private void generateEmployees() throws SQLException {
        ThreadBatchExecutor.ThreadBatchConsumer<PreparedStatement> employeesThreadGenerator =
                (prepared,i, batchManager) -> {
                        int pos = 0;
                        prepared.setString(++pos, employeeName(i));
                        prepared.setInt(++pos, randomPositiveInt(MAX_AGE));
                        prepared.setInt(++pos, randomPositiveInt(MAX_SALARY));
                        prepared.setString(++pos, departmentName(randomPositiveInt(propertiesConfiguration.departments())));
                        batchManager.submit();
                };

        generateInThreadBatch(
                connectionSupplier, propertiesConfiguration.employees(), INSERT_EMPLOYEE_SQL,
                employeesThreadGenerator
        );
    }
}
