package es.aht.pgmongojoins.operation;


import es.aht.pgmongojoins.util.BatchManager;
import es.aht.pgmongojoins.util.PropertiesConfiguration;
import es.aht.pgmongojoins.util.SQLExceptionSupplier;
import es.aht.pgmongojoins.util.ThreadBatchExecutor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;
import java.util.Random;


public abstract class AbstractDataLoader {
    protected final int MAX_FLOORS = 25;
    protected final int MAX_BUDGET = 10_000_000;
    protected final int MAX_AGE = 65;
    protected final int MAX_SALARY = 60_000;
    protected final int PREPARED_STATEMENT_BATCH_SIZE = 10_000;

    protected final PropertiesConfiguration propertiesConfiguration;
    protected final SQLExceptionSupplier<Connection> connectionSupplier;

    private final Random random = new Random();

    public AbstractDataLoader(Properties properties, SQLExceptionSupplier<Connection> connectionSupplier) {
        this.propertiesConfiguration = new PropertiesConfiguration(properties);
        this.connectionSupplier = connectionSupplier;
    }

    public abstract void generateData() throws SQLException;

    protected String departmentName(int departmentNumber) {
        return "Department" + departmentNumber;
    }

    protected String employeeName(int employeeNumber) {
        return "Employee" + employeeNumber;
    }

    protected int randomPositiveInt(int max) {
        return random.nextInt(max) + 1;
    }

    protected void generateInThreadBatch (
            SQLExceptionSupplier<Connection> connectionSupplier, int n, String sql, ThreadBatchExecutor.ThreadBatchConsumer<PreparedStatement> generator
    ) throws SQLException {
        int threads = propertiesConfiguration.threads();
        int blockSize = n / threads;
        if(blockSize < threads) {
            threads = blockSize;
        }

        int lower = 1;
        int upper = blockSize;
        var threadsArray = new ThreadBatchExecutor[threads];
        for(int i = 0; i < threads - 1; i++) {
            threadsArray[i] = new ThreadBatchExecutor(lower, upper, PREPARED_STATEMENT_BATCH_SIZE, connectionSupplier, sql, generator);
            lower += blockSize;
            upper += blockSize;
        }
        threadsArray[threads - 1] = new ThreadBatchExecutor(lower, n, PREPARED_STATEMENT_BATCH_SIZE, connectionSupplier, sql, generator);

        Arrays.stream(threadsArray).forEach(threadBatchExecutor -> threadBatchExecutor.start());
        Arrays.stream(threadsArray).forEach(threadBatchExecutor -> {
            try {
                threadBatchExecutor.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
