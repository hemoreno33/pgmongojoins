package es.aht.pgmongojoins.util;


import java.sql.SQLException;

@FunctionalInterface
public interface SQLExceptionConsumer<T> {
    void accept(T t) throws SQLException;
}
