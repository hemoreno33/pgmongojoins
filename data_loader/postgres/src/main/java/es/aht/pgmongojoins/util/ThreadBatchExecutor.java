package es.aht.pgmongojoins.util;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class ThreadBatchExecutor extends Thread {
    private final int lower;
    private final int upper;
    private final int batchSize;
    private final SQLExceptionSupplier<Connection> supplier;
    private final String sql;
    private final ThreadBatchConsumer<PreparedStatement> consumer;

    public ThreadBatchExecutor(
            int lower, int upper, int batchSize, SQLExceptionSupplier<Connection> supplier, String sql,
            ThreadBatchConsumer<PreparedStatement> consumer
    ) {
        this.lower = lower;
        this.upper = upper;
        this.batchSize = batchSize;
        this.supplier = supplier;
        this.sql = sql;
        this.consumer = consumer;
    }

    @Override
    public void run() {
        try(var connection = supplier.get()) {
            connection.setAutoCommit(false);
            PreparedStatement prepared = connection.prepareStatement(sql);
            BatchManager.doWithBatchManager(connection, prepared, batchSize, batchManager -> {
                for (int i = lower; i <= upper; i++) {
                    consumer.accept(prepared, i, batchManager);
                }
            });
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @FunctionalInterface
    public interface ThreadBatchConsumer<T>  {
        void accept(T t, Integer i, BatchManager batchManager) throws SQLException;
    }
}
