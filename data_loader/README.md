# Data loader

This directory contains the source code of the software used to run the
benchmark described in the blog post. There are two main parts:

* A Java program that generates some random data and loads it into
  Postgres. The program supports some personalization via a
  `pgmongojoins.properties` config file, that needs to be supplied. See
  the companion `pgmongojoins.properties.example` as an example. To
  compile the program, you need Maven, do a `mvn package`. Then run the
  resulting `jar` file and provide the required arguments. Note that the
  program supports concurrent processing for faster loading times.

* A shell script to import the data into MongoDB. To ensure the data,
  even randomly generated, remains the same for both environments, it is
  exported from Postgres and then inserted into MongoDB. Postgres JSON
  functions are heavily leveraged here to export data from Postgres in a
  format suitable for importing into MongoDB.
